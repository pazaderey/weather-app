# -*- coding: utf-8 -*-

import sys
import requests
from PyQt5 import QtWidgets, QtCore

# Импорт библиотеки с классами окон, созданных из .ui файлов
import lib as lb

# Класс основного окна программы
class MainForm(QtWidgets.QMainWindow, lb.MainWindow.Ui_Dialog):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle("Прогноз Погоды")
        self.pushButton.clicked.connect(self.find_weather_clicked)
        self.exitButton.clicked.connect(self.exit_clicked)


    def find_weather_clicked(self):
        '''
        Метод нажатия на кнопку "Узнать погоду".
        Вызывает функцию запроса погоды с параметром "find".
        Если запрос не удался, вызывает окно NoNetForm.
        Если запрос вернул 0 городов, вызывает окно NoCityForm.
        Если запрос вернул 1 город, вызываются функции запроса с параметрами "weather" и "forecast".
        Если запрос вернул больше 1 города, вызывает окно MultiCityForm.
        '''
        s_city = self.textEdit.toPlainText().strip()
        try:
            data = query("find", s_city)
        except:
            self.No_Net_W = NoNetForm()
            self.No_Net_W.show()
            return None
        cities = ["{},{}".format(d["name"], d["sys"]["country"]) for d in data["list"]]
        if len(cities) == 0:
            self.No_Cities_W = NoCityForm()
            self.No_Cities_W.show()
        elif len(cities) == 1:
            s_city = s_city + ',' + data["list"][0]["sys"]["country"]
            weather_request(s_city)
            weather_forecast(s_city)
            s_city = ""
        else:
            self.Multi_City_W = MultiCityForm()
            self.Multi_City_W.show()
            self.Multi_City_W.listWidget.addItem(str(cities))


    def exit_clicked(self):
        '''
        Метод нажатия на кнопку "Выход".
        Завершает работу программы и закрывает все окна.
        '''
        exit()


# Класс окна, информирующего, что если введён неверный город
class NoCityForm(QtWidgets.QDialog, lb.EmptyCitiesError.Ui_Dialog):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle("Ошибка")
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.pushButton.clicked.connect(self.city_er_ok_clicked)


    def city_er_ok_clicked(self):
        '''
        Метод нажатия на кнопку "ОК" в окне NoCityForm.
        Закрывает окно NoCityForm.
        '''
        self.hide()


# Класс окна, информирующего, что произошла ошмбка запроса.
# Скорее всего, это отсутсвтвие интернета
class NoNetForm(QtWidgets.QDialog, lb.NoNetError.Ui_Dialog):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle("Ошибка")
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.pushButton.clicked.connect(self.net_er_ok_clicked)


    def net_er_ok_clicked(self):
        '''
        Метод нажатия на кнопку "ОК" в окне NoNetForm.
        Закрывает окно NoNetForm.
        '''
        self.hide()


# Класс окна, информирующего, что существует несколько городов с одинаоквым названием.
# В нём происходит выбор нужного кода страны
class MultiCityForm(QtWidgets.QDialog, lb.MultiCityError.Ui_Dialog):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle("Выбор Города")
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.pushButton.clicked.connect(self.multi_er_ok_clicked)
        self.listWidget.clear()
        self.textEdit.setText('')


    def multi_er_ok_clicked(self):
        '''
        Метод нажатия на кнопку "ОК" в окне MultiCityForm.
        Вызывает функции запроса с параметрами "weather" и "forecast"
        Если запрос выдал ошибку, вызывает окно CodeErrorForm.
        '''
        if self.textEdit.toPlainText().strip() == '':
            self.Code_W = CodeErrorForm()
            self.Code_W.show()
            return None
        else:
            s_city = main_W.textEdit.toPlainText().strip() + ',' + self.textEdit.toPlainText().strip()
            try:
                weather_request(s_city)
                weather_forecast(s_city)
            except:
                self.Code_W = CodeErrorForm()
                self.Code_W.show()
                return None
            s_city = ''
            self.hide()


# Класс окна, информирующего пользователя, что введён неверный код страны
class CodeErrorForm(QtWidgets.QDialog, lb.CodeError.Ui_Dialog):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle("Ошибка")
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.pushButton.clicked.connect(self.code_er_ok_clicked)


    def code_er_ok_clicked(self):
        '''
        Метод нажатия на кнопку "ОК" в окне CodeErrorForm.
        Закрывает окно CodeErrorForm.
        '''
        self.hide()


def wind_direction(dir):
    '''
    Опрeделение направления ветра по углу.
    На вход - направление в градусах.
    Возвращает - строку с направлением.
    '''
    if ((dir >= 0 and  dir <= 23) or (dir <= 360 and dir > 338)): return "Северный"
    elif (dir > 23 and dir <= 68): return "Северо-Восточный"
    elif (dir > 68 and dir <= 113): return "Восточный"
    elif (dir > 113 and dir <= 158): return "Юго-Восточный"
    elif (dir > 158 and dir <= 203): return "Южный"
    elif (dir > 203 and dir <= 248): return "Юго-Западный"
    elif (dir > 248 and dir <= 293): return "Западный"
    elif (dir > 293 and dir <= 338): return "Северо-Западный"


def weather_request(s_city):
    '''
    Запрос и запись данных о погоде на сегодня в верхний список в главном окне.
    На вход - название города.
    '''
    data = query("weather", s_city)
    main_W.listWidget.clear()
    main_W.listWidget.addItem("Текущая темпеатура: " + str(data["main"]["temp"]) + "°C, " + data["weather"][0]["description"])
    main_W.listWidget.addItem("Минимальная темература за сегодня: " + str(data["main"]["temp_min"]) + "°C")
    main_W.listWidget.addItem("Максимальная температура за сегодня: " + str(data["main"]["temp_max"]) + "°C")
    main_W.listWidget.addItem("Направление и скорость ветра: " + wind_direction(data["wind"]["deg"]) + ", " + str(data["wind"]["speed"]) + " м/с")
    main_W.listWidget.addItem("Текущая влажность: " + str(data["main"]["humidity"]) + "%")
    main_W.listWidget.addItem("Текущее атмосферное давление: " + str(round(int(data["main"]["pressure"])/1.33)) + " мм. рт. ст.")


def weather_forecast(s_city):
    '''
    Запрос и запись данных о прогнозе в нижний список в главном окне.
    На вход - название города.
    '''
    data = query("forecast", s_city)
    main_W.listWidget_2.clear()
    for i in data["list"]:
        if i["dt_txt"][11:13] == "12":
            day = i["dt_txt"][8:10] + '.' + i["dt_txt"][5:7] + '.' + i["dt_txt"][0:4]
            main_W.listWidget_2.addItem('Примерная температура днём: ' + str('{0:+3.0f}'.format(i["main"]["temp"])) + "°C, " + i["weather"][0]["description"])
        elif i["dt_txt"][11:13] == "00":
            day = i["dt_txt"][8:10] + '.' + i["dt_txt"][5:7] + '.' + i["dt_txt"][0:4]
            main_W.listWidget_2.addItem("Погода на " + day + ':' +
                                        "\nПримерная температура ночью: " + str("{0:+3.0f}".format(i["main"]["temp"])) + "°C, " + i["weather"][0]["description"])
        elif i["dt_txt"][11:13] == "06":
            day = i["dt_txt"][8:10] + '.' + i["dt_txt"][5:7] + '.' + i["dt_txt"][0:4]
            main_W.listWidget_2.addItem("Примерная температура утром: " + str("{0:+3.0f}".format(i["main"]["temp"])) + "°C, " + i["weather"][0]["description"])
        elif i["dt_txt"][11:13] == "18":
            day = i["dt_txt"][8:10] + '.' + i["dt_txt"][5:7] + '.' + i["dt_txt"][0:4]
            main_W.listWidget_2.addItem("Примерная температура вечером: " + str("{0:+3.0f}".format(i["main"]["temp"])) + "°C, " + i["weather"][0]["description"])


def query(aim, s_city):
    '''
    Функция запроса данных через API OpenWeatherMap.org.
    На вход - параметр запроса, определяющий количество и тип возвращаемых данных, и название города.
    Возвращает - json объект с информацией о погоде.
    '''
    url = "http://api.openweathermap.org/data/2.5/"
    app_id = "85cad0c08b0eb08ea8830123a11e395e"
    res = requests.get(url+aim, params={"q": s_city, "units": "metric", "lang": "ru", "APPID": app_id})
    data = res.json()
    return data


# Главная функция
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    main_W = MainForm()
    main_W.show()
    app.exec_()
